<?php
/**
 * Single post template.
 */

get_header();

// Start the Loop.
while ( have_posts() ) {
	the_post();
	global $post_id;
	$pid = get_the_ID();
	$descripcion_corta     = get_the_excerpt();
	$tipo_de_articulo      = get_field( 'tipo_de_articulo' );
	$enlace_de_inscripcion = get_field( 'enlace_de_inscripcion' );
	$fecha_de_inicio       = get_field( 'fecha_de_inicio' );
	$post_date             = get_the_date( 'j\. m\. Y' );
	$download_pdf          = get_field( 'download_pdf' );
	?>
	<!-- Main content -->
	<div class="o-single" id="generatePdf">
		<div class="o-single__main">
			<div class="o-single__container o-container">
				<div class="o-single__inner">
					<article class="o-single__article">
						<header class="o-single__header">
							<?php
							$post_categories = wp_get_post_categories(get_the_ID(), array(
								'exclude' => 1 // exclude the "Uncategorized" category by ID
							));
							if ( $post_categories ) {
								echo '<div class="c-card__tax';
								if ( 'evento' == get_post_type( $pid ) && ! empty( $fecha_de_inicio ) ) {
									echo ' c-card__tax--evento';
								}
								echo '">';
								foreach ( $post_categories as $category_id ) {
									$category = get_category( $category_id );
									echo '<span class="c-card__tax-separador">' . esc_html( $category->name ) . '</span>';
								}
								echo '</div>';
							}
							?>
							<h1 class="c-single-header__title"><?php the_title(); ?></h1>
							<?php
							if ( 'evento' == get_post_type() && ! empty( $fecha_de_inicio ) ) {
								echo '<div class="c-card__meta--evento"><span class="c-card__meta--evento-dia">' . date( 'd', strtotime( $fecha_de_inicio ) ) . '</span><span class="c-card__meta--evento-mes">' . date( 'M', strtotime( $fecha_de_inicio ) ) . '</span></div>';
							} else {
								echo '<div class="c-card__meta c-card__meta--single">' . esc_html( $post_date ) . '</div>';
							}
							?>
							<?php
							if ( 'evento' === $tipo_de_articulo && ! empty( $enlace_de_inscripcion ) ) {
								?>
								<a href="<?php echo esc_url( $enlace_de_inscripcion ); ?>" target="_blank"
								   class="o-button" style="margin-top: 32px!important;">Enlace de inscripción</a>
								<?php
							}
							?>
						</header>
						<div class="o-single__content-wrapper">
							<div class="o-single__content">
								<?php
								if ( has_post_thumbnail() ) {
									?>
									<div class="o-single__featured-image-wrapper">
										<div class="o-single__featured-image">
											<?php the_post_thumbnail(); ?>
											<?php
											$featured_image_caption = get_the_post_thumbnail_caption();
											if ( $featured_image_caption ) {
												?>
												<div class="o-single__featured-image-caption">
													<?php
													the_post_thumbnail_caption();
													?>
												</div>
												<?php
											}
											?>
										</div>
									</div>
									<?php
								}
								?>

								<?php get_template_part( 'template-parts/share-links' ); ?>

								<?php
								if ( ! empty( get_the_content() ) ) {
									?>
									<div class="o-content-from-editor js-content-from-editor">
										<?php the_content(); ?>
										<?php
										if ( ! empty( $download_pdf ) ) {
											?>
											<a href="<?php echo $download_pdf['url']; ?>" class="o-button o-button--pdf"
											   title="<?php echo $download_pdf['title']; ?>" target="_blank">Descargar
												PDF</a>
											<?php
										}
										?>
									</div>
									<?php
								}
								?>
							</div>


							<aside class="o-single__sidebar c-sidebar">
								<!--Contenido relacionado-->
								<?php get_template_part( 'template-parts/ultimas-publicaciones' ); ?>
							</aside>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
	<?php
}
get_footer();
