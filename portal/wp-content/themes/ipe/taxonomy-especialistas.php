<?php
get_header();
?>

	<section>
		<article>

			<header class="c-page-header">
				<div class="c-page-header__container o-container">

					<div class="c-page-header__wrapper">
						<div class="c-page-header__inner">
							<div class="c-page-header__heading">
								<?php
								the_archive_title( '<h1 class="c-page-header__heading">', '</h1>' );
								the_archive_description( '<div class="c-page-header__description">', '</div>' );
								?>
							</div>
						</div>
					</div>
				</div>
			</header>


			<div class="o-single__container">

				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					$pid = get_queried_object()->term_id;;
					$args       = array(
						'tax_query' => array(
							array(
								'taxonomy' => 'especialistas',
								'field'    => 'term_id',
								'terms'    => $pid,
							),
						),
					);
					$blog_posts = get_posts( $args );

					if ( $blog_posts ) {

						get_template_part(
							'template-parts/card-block',
							null,
							array(
								'items'          => $blog_posts,
								'disable-images' => true,
								'layout'         => 'standard',
							)
						);
					}

				endwhile;
				?>
			</div>
		</article>
	</section>


<?php
get_footer();
