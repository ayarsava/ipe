<?php
/**
 * Footer template
 */


$btn_label = get_field( 'button_label', 'options' );
$btn_url   = get_field( 'button_url', 'options' );

?>
</main>

<footer class="c-footer">
	<div class="o-container">
		<div class="c-footer__inner">
			<div class="c-footer__logo">
				<img src="<?php echo get_template_directory_uri() . '/assets/img/logo-ipe.svg' ?>" height="28">
			</div>
			<?php
			$shortcode = get_field( 'shortcode_del_formulario', 'options' );
			if ( ! empty( $shortcode ) ) {
				?>

				<div class="c-newsletter-block__shortcode">
					<?php
					echo do_shortcode( $shortcode );
					?>
				</div>
				<?php
			}
			?>

			<?php
			$email = get_field( 'email', 'options' );
			if ( ! empty( $email ) ) {
				?>
				<div class="c-footer__btn">
					<a href="mailto:<?php echo esc_html__( $email ); ?>" class="c-footer__btn-link" target="_blank">Contacto</a>
				</div>
				<?php
			}
			?>
		</div>


		<div class="c-footer__bottom-links">
			<div class="c-footer__bottom-container">

				<?php
				$twitter   = get_field( 'twitter', 'option' );
				$facebook  = get_field( 'facebook', 'option' );
				$instagram = get_field( 'instagram', 'option' );
				$linkedin  = get_field( 'linkedin', 'option' );
				$credits   = get_field( 'credits_line', 'option' );
				?>

				<div class="c-footer__social">
					<?php
					if ( $instagram ) {
						?>
						<a href="<?php echo esc_url( $instagram ); ?>" title="Visit us on Instagram"
						   class="c-footer__social-btn c-footer__social-btn--li" target="_blank">
							<img src="<?php echo get_template_directory_uri() . '/assets/img/instagram.svg' ?>"
								 height="16">
						</a>
						<?php
					}
					if ( $linkedin ) {
						?>
						<a href="<?php echo esc_url( $linkedin ); ?>" title="Visit us on Linkedin"
						   class="c-footer__social-btn c-footer__social-btn--li" target="_blank">
							<img src="<?php echo get_template_directory_uri() . '/assets/img/linkedin.svg' ?>"
								 height="16">
						</a>
						<?php
					}
					if ( $twitter ) {
						?>
						<a href="<?php echo esc_url( $twitter ); ?>" title="Visit us on Twitter"
						   class="c-footer__social-btn c-footer__social-btn--li" target="_blank">
							<img src="<?php echo get_template_directory_uri() . '/assets/img/twitter.svg' ?>"
								 height="16">
						</a>
						<?php
					}
					if ( $facebook ) {
						?>
						<a href="<?php echo esc_url( $facebook ); ?>" title="Visit us on Facebook"
						   class="c-footer__social-btn c-footer__social-btn--fb" target="_blank">
							<img src="<?php echo get_template_directory_uri() . '/assets/img/facebook.svg' ?>"
								 height="16">
						</a>
						<?php
					}
					?>
				</div>

				<?php if ( $credits ) {
					?>

					<div class="c-footer__credit">
					<div class="c-footer__credit-link"><?php echo esc_html( $credits ); ?><?php echo esc_attr( gmdate( 'Y' ) ); ?></div>
					</div><?php
				}
				?>
			</div>
		</div>
	</div>
</footer>

<?php
wp_footer();
?>
</body>
</html>
