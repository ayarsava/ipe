<?php
/**
 * The header of the whole website.
 */
?>

<a class="c-skip-button" href="#content" title="Skip to content">
	Skip to content
</a>

<header class="c-header js-header">
	<div class="o-container">
		<div class="c-header__inner">

			<form class="c-header__search_box--mobile"
				  action="<?php echo esc_url( home_url( '?s=' ) ); ?>"
				  method="get" role="search">
				<label class="c-search-form__label" for="search-input">Buscador por palabra</label>

				<input class="c-header__search__input" id="search-input" name="s" type="text"
					   placeholder="Buscar"/>
				<button class="c-header__submit" title="Buscar" type="submit"
						aria-label="Buscar"><img
							src="<?php echo get_template_directory_uri() . '/assets/img/arrow.svg' ?>"
							height="14">
				</button>
			</form>

			<button class="c-header__burger js-burger-button" aria-label="Abrir el menú">
				<span></span><span></span><span></span>
			</button>

			<a class="c-header__logo js-header-logo" href="<?php echo esc_url( home_url() ); ?>" title="Go to homepage">
				<img src="<?php echo get_template_directory_uri() . '/assets/img/logo-ipe.svg' ?>" height="28">
			</a>

			<button class="c-header__search-mobile" aria-label="Buscar">
				<img src="<?php echo get_template_directory_uri() . '/assets/img/search.svg' ?>"
					 height="21">
			</button>

			<div class="c-header__contents js-burger-content">

				<div class="c-header__menu-wrapper">
					<nav class="c-main-menu js-main-menu">
						<form class="c-header__search_box"
							  action="<?php echo esc_url( home_url( '?s=' ) ); ?>"
							  method="get" role="search">
							<label class="c-search-form__label" for="search-input">Buscador por palabra</label>

							<input class="c-header__search__input" id="search-input" name="s" type="text"
								   placeholder="Buscar"/>
							<button class="c-header__submit" title="Buscar" type="submit"
									aria-label="Buscar"><img
										src="<?php echo get_template_directory_uri() . '/assets/img/arrow.svg' ?>"
										height="14">
							</button>
						</form>
						<div class="c-main-menu__container">
							<?php
							$main_menu_args = array(
								'theme_location'  => 'main-menu',
								'container_class' => 'c-header__menu-wrapper',
								'menu_class'      => 'c-main-menu__list',
								'walker'          => new Main_Menu_Walker(),
								'depth'           => 2,
							);

							wp_nav_menu( $main_menu_args );
							?>

							<div class="c-header__search-wrapper">
								<button class="c-header__search-controls js-search-open" aria-label="Search">
									<span class="c-header__search-label">Search</span>
									<span class="c-header__search-button">
							<img src="<?php echo get_template_directory_uri() . '/assets/img/search.svg' ?>"
								 height="21">
						</span>
								</button>
							</div>
						</div>

					</nav>
				</div>
			</div>
		</div>
	</div>
</header>

<main id="content" class="c-main-content js-content">
