<?php
$classes = $args['classes'] ?? '';
?>

<form class="c-search-form <?php echo esc_attr( $classes ); ?>" action="<?php echo esc_url( home_url( '/' ) ); ?>"
	  method="get" role="search">
	<label class="c-search-form__label" for="search-input">Search all content</label>

	<input class="c-search-form__input" id="search-input" name="s" type="text" placeholder="Search"/>

	<button class="c-search-form__button" title="Search all content" type="submit" aria-label="Search">
		<?php
		get_template_part(
			'assets/views/svg',
			null,
			array(
				'classes' => 'c-search-form__icon js-search-icon',
				'icon'    => 'right-arrow',
			)
		);
		?>
	</button>
</form>
