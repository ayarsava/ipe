<?php
if ( empty( $args['post_id'] ) ) {
	$args['post_id'] = get_the_ID();
}
$post_id           = $args['post_id'];
$short_description = get_field( 'short_description', $post_id );
$classes           = $args['classes'] ?? '';
$has_image         = false;

if ( has_post_thumbnail() ) {
	$classes   .= ' c-pageº-header--background';
	$has_image = true;
}
?>

<header class="c-page-header <?php echo esc_attr( $classes ); ?>">
	<div class="c-page-header__container o-container">

		<div class="c-page-header__wrapper">
			<div class="c-page-header__inner">
				<h1 class="c-page-header__heading">
					<?php echo esc_html( get_the_title( $post_id ) ); ?>
				</h1>
				<?php
				if ( ! empty( $short_description ) ) {
					?>
					<div class="c-page-header__description o-content-from-editor">
						<?php echo wp_kses( $short_description, 'post' ); ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>

		<?php
		if ( $has_image ) {
			?>
			<div class="c-page-header__featured-image-wrapper">
				<div class="c-page-header__featured-image">
					<?php the_post_thumbnail(); ?>
				</div>
			</div>
			<?php
		}
		?>
	</div>
</header>
