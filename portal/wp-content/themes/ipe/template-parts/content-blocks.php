<?php
$pid        = $args['pid'] ?? get_the_ID();
$components = get_field( 'components', $pid );

if ( ! empty( $components ) ) {
	foreach ( $components as $block ) {
		switch ( $block['acf_fc_layout'] ) {
			case 'call_to_action':
				$template = 'template-parts/components/call-to-action';

				break;

			case 'listing_block':
				$template = 'template-parts/components/listing-block';

				break;

			case 'featured_content':
				$template = 'template-parts/components/featured-content';

				break;

			case 'text_block':
				$template = 'template-parts/components/text-block';

				break;

			case 'newsletter_block':
				$template = 'template-parts/components/newsletter-block';

				break;

			case 'image_slider':
				$template = 'template-parts/components/image-slider';

				break;

			case 'especialistas':
				$template = 'template-parts/components/especialistas';

				break;

			case 'logo_block':
				$template = 'template-parts/components/logo-block';

				break;

			/*
			case 'downloads':
				$template = 'template-parts/components/downloads';

				break;

			case 'upcoming_events':
				$template = 'template-parts/components/upcoming-events';

				break;
			*/

			default:
				// Component doesn't exist, move to the next one.
				$template = null;
				continue 2;
		}

		$args = array(
			'block' => $block,
		);

		if ( isset( $template ) && ! empty( $template ) ) {
			get_template_part( $template, null, $args );
		}
	}
}
