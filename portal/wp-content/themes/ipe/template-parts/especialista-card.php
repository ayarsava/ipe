<?php
/**
 * People card
 */
if ( ! empty( $args['pid'] ) ) {
	$class       = '';
	$pid         = $args['pid'];
	$url         = get_term_link( $pid );
	$full_name   = get_term( $pid )->name;
	$description = get_term( $pid )->description;
	$term_id     = get_term( $pid );
	$headshot    = get_field( 'headshot', $pid );
	$image_id    = get_field( 'headshot', $term_id, false );
	$image       = wp_get_attachment_image_src( $image_id, 'full' );
	if ( empty( $headshot ) ) {
		$class = 'c-card--especialista__placeholder';
	}
	?>
	<a class="c-card c-card--especialista <?php echo esc_attr( $class ); ?>"
	   href="<?php echo esc_url( $url ); ?>"
	   title="<?php echo esc_attr( $full_name ); ?>">
		<div class="c-card__arrow">
			<img src="<?php echo get_template_directory_uri() . '/assets/img/goto.svg' ?>"
				 class="c-card__arrow-image">
		</div>
		<?php
		if ( ! empty( $headshot ) ) {
			?>
			<div class="c-card--especialista__image-container">
				<img src="<?php echo $image[0]; ?>" class="c-card--especialista__image">
			</div>
			<?php
		}
		?>
		<div class="c-card--especialista__content">
			<h3 class="c-card--especialista__title">
				<?php
				echo esc_html( $full_name );
				?>
			</h3>
			<div class="c-card--especialista__description">
				<?php echo esc_html( $description ); ?>
			</div>
		</div>
	</a>
	<?php
}
