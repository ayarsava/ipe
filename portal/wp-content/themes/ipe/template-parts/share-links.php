<?php
/**
 * Generate the social media sharer buttons.
 * - Achieved without loading each platform's scripts and tracking.
 * - Based on https://jonsuh.com/blog/social-share-links/.
 */

$pid            = $args['post_id'] ?? get_the_ID();
$site_title     = get_bloginfo( 'name' );
$post_title     = get_the_title( $pid );
$post_link      = wp_get_shortlink( $pid );
$excerpt        = get_page_builder_excerpt( $pid, 180 );
$twitter_handle = get_field( 'twitter_handle', 'option' );

$twitter_share_link = '//twitter.com/share?' . http_build_query(
		array(
			'text' => $post_title,
			'url'  => $post_link,
			'via'  => ltrim( $twitter_handle, '@' ),
		)
	);

$linkedin_share_link = '//www.linkedin.com/shareArticle?' . http_build_query(
		array(
			'mini'    => 'true',
			'url'     => $post_link,
			'title'   => $post_title,
			'summary' => $excerpt,
			'source'  => $site_title,
		)
	);

$facebook_share_link = '//www.facebook.com/sharer/sharer.php?' . http_build_query(
		array(
			'u' => $post_link,
			't' => $post_title,
		)
	);

$email_share_link = 'mailto:?' . http_build_query(
		array(
			'subject' => $post_title,
			'body'    => $post_title . '. ' . $post_link,
		)
	);

$links = array(

	'twitter'  => array(
		'alt_text' => 'Twitter',
		'icon'     => 'twitter',
		'link'     => $twitter_share_link,
		'popup'    => true,
	),
	'facebook' => array(
		'alt_text' => 'Facebook',
		'icon'     => 'facebook',
		'link'     => $facebook_share_link,
		'popup'    => true,
	),
	'email'    => array(
		'alt_text' => 'email',
		'icon'     => 'email',
		'link'     => $email_share_link,
		'popup'    => false,
	),
	/*	'linkedin' => array(
			'alt_text' => 'LinkedIn',
			'icon'     => 'linkedin',
			'link'     => $linkedin_share_link,
			'popup'    => true,
		),*/
);
?>

<div class="c-share">
	<div class="c-share__links">
		<?php
		foreach ( $links as $k => $post_link ) {
			if ( $post_link['popup'] ) {
				$js_class = 'js-social-share';
				$target   = '_blank';
			} else {
				$js_class = '';
				$target   = '_self';
			}
			?>
			<a class="c-share__link <?php echo esc_attr( $js_class ); ?> c-share__link-<?php echo $post_link['icon']; ?>"
			   href="<?php echo esc_attr( $post_link['link'] ); ?>" target="<?php echo esc_attr( $target ); ?>"
			   title="Share on <?php echo esc_attr( $post_link['alt_text'] ); ?>">
				<svg>
					<use xlink:href="#<?php echo $post_link['icon']; ?>">
				</svg>
			</a>
			<?php
		}
		?>
	</div>
</div>

<svg display="none">
	<symbol width="11" height="21" viewBox="0 0 11 21" id="facebook">
		<path fill-rule="evenodd" clip-rule="evenodd"
			  d="M7.18639 20.6746V11.2605H10.4056L10.866 7.58728H7.18639V5.29089C7.18639 4.25764 7.53172 3.45429 9.02555 3.45429H10.9798V0.124658C10.5207 0.124658 9.37087 0.00970893 8.10597 0.00970893C7.46613 -0.0326561 6.82469 0.0639632 6.22583 0.292914C5.62697 0.521864 5.08491 0.877706 4.63698 1.33594C4.18905 1.79417 3.8459 2.3439 3.63115 2.94728C3.41639 3.55066 3.33514 4.19335 3.39298 4.8311V7.58728H0.173828V11.2605H3.39298V20.6746H7.18639Z"
			  fill="currentColor"/>
	</symbol>

	<symbol width="21" height="17" viewBox="0 0 21 17" id="twitter">
		<path fill-rule="evenodd" clip-rule="evenodd"
			  d="M6.55343 17.345C8.14269 17.3593 9.71889 17.0572 11.1899 16.4564C12.661 15.8557 13.9974 14.9682 15.1212 13.8459C16.245 12.7235 17.1336 11.3888 17.735 9.91977C18.3365 8.45068 18.6388 6.87665 18.6243 5.28959V4.71614C19.4276 4.1054 20.1264 3.3687 20.6937 2.5347C19.9226 2.86937 19.1107 3.10084 18.279 3.2231C19.134 2.68689 19.7822 1.87753 20.1181 0.926706C19.2876 1.38854 18.3983 1.73611 17.4745 1.95995C17.0854 1.52264 16.6071 1.17352 16.0717 0.936054C15.5363 0.698585 14.9563 0.578257 14.3705 0.583151C13.2476 0.600147 12.1755 1.05315 11.3814 1.84614C10.5873 2.63913 10.1336 3.70976 10.1166 4.83109C10.0905 5.14185 10.1297 5.45463 10.2317 5.74938C8.53985 5.66785 6.88497 5.22614 5.37798 4.45386C3.87098 3.68159 2.54668 2.59658 1.49382 1.27155C1.11547 1.93637 0.917528 2.68832 0.919573 3.45299C0.934746 4.14694 1.1083 4.82827 1.42701 5.44511C1.74572 6.06196 2.20119 6.59803 2.75872 7.01253C2.06779 6.99858 1.39312 6.80061 0.804469 6.43908C0.803392 7.4224 1.14816 8.37488 1.77859 9.13025C2.40902 9.88562 3.28508 10.3959 4.25384 10.5721C3.87946 10.6729 3.49103 10.7118 3.10405 10.687C2.8308 10.7053 2.55673 10.6662 2.29958 10.5721C2.58067 11.4229 3.11805 12.1662 3.83825 12.7005C4.55846 13.2348 5.42633 13.5338 6.32321 13.5569C4.81193 14.7356 2.95229 15.3815 1.03468 15.3934C0.685808 15.4121 0.33624 15.3733 0 15.2785C1.90258 16.6582 4.20245 17.3834 6.55343 17.345Z"
			  fill="currentColor"/>
	</symbol>

	<symbol width="28" height="19" viewBox="0 0 28 19" id="email">
		<path d="M20 4.28v6.657c0 .863-.84 1.563-1.875 1.563H1.875C.84 12.5 0 11.8 0 10.937V4.284c0-.162.222-.254.379-.153.875.566 2.035 1.287 6.02 3.698.823.502 2.215 1.555 3.601 1.55 1.395.01 2.813-1.069 3.605-1.55 3.985-2.411 5.141-3.134 6.016-3.7.153-.1.379-.007.379.152M7.132 6.985C1.95 3.85 1.555 3.573.36 2.795.132 2.646 0 2.423 0 2.18v-.618C0 .7.84 0 1.875 0h16.25C19.16 0 20 .699 20 1.563v.617c0 .242-.132.47-.36.616-1.195.781-1.59 1.054-6.772 4.19-.657.396-1.962 1.36-2.868 1.347-.906.013-2.211-.95-2.868-1.348"
			  fill="currentColor" fill-rule="evenodd"/>
	</symbol>
</svg>
