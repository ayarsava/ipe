<?php
/**
 * Card component.
 */

if ( ! empty( $args['pid'] ) ) {
	$pid = $args['pid'];

	if ( $pid instanceof WP_Post ) {
		$pid = $pid->ID;
	}

	$class            = $args['class'] ?? '';
	$card_link        = get_the_permalink( $pid );
	$card_title       = get_the_title( $pid );
	$card_desc        = get_the_excerpt( $pid );
	$post_date        = get_the_date( 'j \d\e F \d\e Y', $pid );
	$disable_images   = $args['disable-images'] ?? '';
	$disable_desc     = $args['disable-desc'] ?? false;
	$publication_date = get_field( 'publication_date', $pid );
	$content_type     = get_post_type( $pid );
	$image            = get_the_post_thumbnail( $pid );
	$fecha_de_inicio  = get_field( 'fecha_de_inicio', $pid );

	?>
	<a href="<?php echo esc_url( $card_link ); ?>" class="c-card <?php echo esc_attr( 'c-card' . $class ); ?>">
		<?php
		if ( ! $disable_images ) {
			?>
			<?php if ( $image ) { ?>
				<div class="c-card__image-container">
					<?php
					if ( has_term( 'video', 'tipos-de-contenido' ) ) {
						?>
						<div class="c-card__image-hover">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26">
								<polygon class="play-btn__svg" points="9.33 6.69 9.33 19.39 19.3 13.04 9.33 6.69"/>
								<path class="play-btn__svg"
									  d="M26,13A13,13,0,1,1,13,0,13,13,0,0,1,26,13ZM13,2.18A10.89,10.89,0,1,0,23.84,13.06,10.89,10.89,0,0,0,13,2.18Z"/>
							</svg>
						</div>
						<?php
					}
					echo get_the_post_thumbnail(
						$pid,
						'full-width-uncropped',
						array( 'class' => 'c-card__image' )
					);
					?>

				</div>
				<?php
			}
			?>
			<?php
		}
		?>

		<div class="c-card__content">
			<div class="c-card__arrow">
				<img src="<?php echo get_template_directory_uri() . '/assets/img/goto.svg' ?>"
					 class="c-card__arrow-image">
			</div>


			<?php
			if ( 'evento' == get_post_type( $pid ) && ! empty( $fecha_de_inicio ) ) {
				echo '<div class="c-card__meta--evento"><span class="c-card__meta--evento-dia">' . date( 'd', strtotime( $fecha_de_inicio ) ) . '</span><span class="c-card__meta--evento-mes">' . date( 'M', strtotime( $fecha_de_inicio ) ) . '</span></div>';
			}
			?>


			<?php



			if ( 'evento' != get_post_type( $pid ) && empty( $fecha_de_inicio ) ) {
				echo '<div class="c-card__meta">' . esc_html( $post_date ) . '</div>';
			}
			?>

			<div class="c-card__title">
				<?php echo esc_html( $card_title ); ?>
			</div>
			<?php
			// if ( ! empty( $card_desc ) && '--x3' !== $class && '--x4' !== $class && false === $disable_desc ) {
			if ( ! empty( $card_desc ) && false === $disable_desc ) {
				?>
				<div class="c-card__desc">
					<?php echo substr( $card_desc, 0, 200 ); ?>
				</div>
				<?php
			}
			?>
		</div>
	</a>
	<?php
}
