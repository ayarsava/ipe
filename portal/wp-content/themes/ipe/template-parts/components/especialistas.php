<?php
/**
 * Especialistas
 */
if ( ! empty( $args['block'] ) ) {
	$block         = $args['block'];
	$slider_title  = $block['title'];
	$description   = $block['description'];
	$button        = $block['button'] ?? '';
	$button_target = $button['target'] ?? '_self';
	$items         = $block['especialistas'];
	$random_order  = $block['random_order'];


	if ( $random_order ) {
		shuffle( $items );
	}
	?>
	<section class="c-especialistas o-section">
		<div class="c-especialistas__container o-container">
			<div class="c-especialistas__heading-wrapper">
				<?php
				if ( ! empty( $slider_title ) ) {
					?>
					<h2 class="c-especialistas__title">
						<?php echo esc_html( $slider_title ); ?>
					</h2>
					<?php
				}

				if ( ! empty( $description ) ) {
					?>
					<div class="c-especialistas__sub-title">
						<?php echo wp_kses( $description, 'post' ); ?>
					</div>
					<?php
				}

				if ( ! empty( $button ) ) {
					?>
					<div class="c-especialistas__button-wrapper o-container">
						<a class="c-especialistas__button o-button" href="<?php echo esc_url( $button['url'] ); ?>"
						   target="<?php echo esc_attr( $button['target'] ); ?>"
						   title="<?php echo esc_attr( $button['title'] ); ?>">
							<?php
							echo esc_html( $button['title'] );

							get_template_part( 'assets/views/svg', null, array( 'icon' => 'arrow' ) );
							?>
						</a>
					</div>
					<?php
				}
				?>
			</div>
		</div>

		<div class="c-especialistas__slider">
			<div class="c-especialistas__slider-inner">

				<?php
				if ( ! empty( $items ) ) {
					?>
					<div class="c-especialistas__container">
						<div class="c-especialistas__slider-cards-wrapper">
							<div class="c-especialistas__slides">
								<?php
								foreach ( $items as $item_id ) {
									get_template_part(
										'template-parts/especialista-card',
										null,
										array(
											'pid' => $item_id,
										)
									);
								}
								?>
							</div>
						</div>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</section>
	<?php
}
