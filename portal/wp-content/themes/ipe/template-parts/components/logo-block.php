<?php
/**
 * Logo block
 */
if ( ! empty( $args['block'] ) ) {
	$block         = $args['block'];
	$block_title   = $block['title'];
	$description   = $block['description'];
	$button        = $block['button'] ?? '';
	$button_target = $button['target'] ?? '_self';
	$items         = $block['logos'];
	$random_order  = $block['random_order'];

	if ( $random_order ) {
		shuffle( $items );
	}
	?>
	<section class="c-logo-block o-section">
		<div class="c-logo-block__container o-container">
			<div class="c-logo-block__heading-wrapper">
				<?php
				if ( ! empty( $block_title ) ) {
					?>
					<h2 class="c-logo-block__title">
						<?php echo esc_html( $block_title ); ?>
					</h2>
					<?php
				}

				if ( ! empty( $description ) ) {
					?>
					<div class="c-logo-block__sub-title">
						<?php echo wp_kses( $description, 'post' ); ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>

		<div class="c-logo-block__cards">
			<div class="c-logo-block__inner">

				<?php
				if ( ! empty( $items ) ) {
					?>
					<div class="c-logo-block__container">
						<div class="c-logo-block__cards-wrapper">
							<?php
							foreach ( $items as $item ) {
								?>
								<div class="c-logo-block__card">
									<?php
									if ( $item['logo_url'] ) { ?>
									<a href="<?php echo esc_url( $item['logo_url'] ); ?>"
									   title="<?php echo esc_attr( $item['logo_title'] ); ?>" target="_blank">
										<?php }
										$image = $item['logo_image']['url'];
										if ( ! empty( $image ) ) { ?>
											<img src="<?php echo $image; ?>" class="c-logo-block__image">
										<?php } ?>
										<?php
										if ( $item['logo_url'] ) { ?>
									</a>
								<?php } ?>
								</div>
								<?php
							}
							?>
						</div>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</section>
	<?php
}
