<?php

if ( ! empty( $args['block'] ) ) {
	$block   = $args['block'];
	$classes = $args['classes'] ?? '';

	if ( ! empty( $block['text'] ) || ! empty( $block['button'] ) ) {
		$text        = $block['text'];
		$description = $block['description'];
		$button      = $block['button'];
		$layout      = $block['layout'] ?: 'destacado';
		?>
		<section class="c-call-to-action<?php echo esc_attr( $classes );
		echo ' c-call-to-action--' . esc_attr( $layout ) ?> o-section">
			<div class="c-call-to-action__container o-container">
				<div class="c-call-to-action__inner">
					<div class="c-call-to-action__content">
						<h2 class="c-call-to-action__title">
							<?php echo esc_html( $text ); ?>
						</h2>

						<div class="c-call-to-action__description">
							<?php echo wp_kses( $description, 'post' ); ?>
						</div>
					</div>
					<?php
					if ( $button ) {
						?>
						<div class="c-call-to-action__right">
							<a class="c-call-to-action__button o-button" href="<?php echo esc_attr( $button['url'] ) ?>"
							   target="<?php echo esc_attr( $button['target'] ) ?>"
							   title="<?php echo esc_attr( $button['title'] ); ?>">
								<?php echo esc_html( $button['title'] ); ?>
							</a>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</section>
		<?php
	}
}
