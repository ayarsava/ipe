<?php

if ( ! empty( $args['block'] ) ) {
	$block      = $args['block'];
	$extra_args = $args['extra_args'] ?? array();

	// Settings.
	$number_of_items = $block['number_of_items_to_show'];
	$auto_enabled    = $block['automatic_content_selection_enabled'];

	// Layout.
	$layout = $block['layout'];

	if ( 'three' === $layout || 'two' === $layout ) {
		$disable_images = $block['disable_images'] ?? false;
		$title_position = $block['title_position'] ?? null;
	} else {
		$disable_images = false;
		$title_position = null;
	}
	// Manual Content.
	$manual_content = $block['manual_featured_content'];

	// Auto Content.
	$content_types_to_show = $block['content_types_to_show'];

	// Taxonomy Filters.
	$taxonomy_filters = array(
		'category' => $block['categories_to_show'] ?? array(),
		// 'topics'              => $block['topics_to_show'] ?? array(),
		// 'countries'           => $block['countries_to_show'] ?? array(),
		// 'languages'           => $block['languages_to_show'] ?? array(),
		// 'regions'             => $block['regions_to_show'] ?? array(),
		// 'survey-rounds'       => $block['survey_rounds_to_show'] ?? array(),
		// 'article-subtype'     => $block['article_type_to_show'] ?? array(),
		// 'publication-subtype' => $block['publication_type_to_show'] ?? array(),
	);

	$taxonomy_filters = array_filter( $taxonomy_filters );
	if ( ! empty( $taxonomy_filters ) ) {
		foreach ( $taxonomy_filters as $taxonomy_slug => $terms ) {
			$extra_args['tax_query'][] = array(
				'taxonomy' => $taxonomy_slug,
				'field'    => 'term_id',
				'terms'    => $terms,
			);
		}
	}

	// Only for events.
	$events_to_show = $block['events_to_show_featured_component'];

	// Prep variables for featured content.

	if ( empty( $manual_content ) ) {
		$manual_content = array();
	}

	if ( empty( $content_types_to_show ) ) {
		$content_types_to_show = array();
	} elseif ( 'event' === $content_types_to_show[0] && ! empty( $events_to_show ) && $auto_enabled ) {

		$current_date = new DateTime();
		$current_date = $current_date->format( 'Y-m-d H:i:s' );

		// phpcs:disable
		$args = array_merge( $args, [
			'post_type'   => $content_types_to_show[0],
			'post_status' => 'publish',
			'fields'      => 'ids',
			'meta_key'    => 'sb_post_date',
			'meta_type'   => 'DATE',
			'orderby'     => 'meta_value',
			'order'       => 'ASC',
		] );

		$args['meta_query']['relation'] = 'AND';
		// phpcs:enable

		if ( 'future' === $events_to_show ) {
			$args['meta_query'][] = array(
				'key'     => 'start_date',
				'value'   => $current_date,
				'compare' => '>',
			);
		}

		if ( 'past' === $events_to_show ) {
			$args['meta_query'][] = array(
				'key'     => 'start_date',
				'value'   => $current_date,
				'compare' => '<=',
			);
		}

		$result_query     = new WP_Query( $args );
		$featured_content = $result_query->posts;
		wp_reset_postdata();

	} else {
		$featured_content = get_featured_content( $manual_content, $auto_enabled, $content_types_to_show, $number_of_items, $extra_args );
	}

	if ( 'true' !== $auto_enabled ) {
		$featured_content = get_featured_content( $manual_content, $auto_enabled, $content_types_to_show, $number_of_items, $extra_args );
	}
	if ( is_array( $featured_content ) && ! empty( $featured_content ) ) {
		get_template_part(
			'template-parts/card-block',
			null,
			array(
				'title'          => $block['title'],
				'items'          => $featured_content,
				'button'         => $block['button'],
				'layout'         => $layout,
				'disable-images' => $disable_images,
				'title_position' => $title_position,
				'description'    => $block['description'],
				'background'     => $block['background_color'],
			)
		);
	}
}
