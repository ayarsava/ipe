<?php
/**
 * Logo slider
 */
if ( ! empty( $args['block'] ) ) {
	$block         = $args['block'];
	$slider_title  = $block['title'];
	$description   = $block['description'];
	$button        = $block['button'];
	$button_target = $button['target'] ?? '_self';
	$items         = $block['slides'];
	$random_order  = $block['random_order'];


	if ( $random_order ) {
		shuffle( $items );
	}
	?>
	<section class="c-image-slider o-section">
		<div class="c-image-slider__container">
			<div class="c-image-slider__heading-wrapper">
				<?php
				if ( ! empty( $slider_title ) ) {
					?>
					<h2 class="c-image-slider__title">
						<?php echo esc_html( $slider_title ); ?>
					</h2>
					<?php
				}

				if ( ! empty( $description ) ) {
					?>
					<div class="c-image-slider__sub-title">
						<?php echo wp_kses( $description, 'post' ); ?>
					</div>
					<?php
				}

				if ( ! empty( $button ) ) {
					?>
					<div class="c-image-slider__button-wrapper o-container">
						<a class="c-image-slider__button o-button" href="<?php echo esc_url( $button['url'] ); ?>"
						   target="<?php echo esc_attr( $button['target'] ); ?>"
						   title="<?php echo esc_attr( $button['title'] ); ?>">
							<?php
							echo esc_html( $button['title'] );

							get_template_part( 'assets/views/svg', null, array( 'icon' => 'arrow' ) );
							?>
						</a>
					</div>
					<?php
				}
				?>
				<div class="c-image-slider__slider">
					<div class="c-image-slider__slider-inner">

						<?php
						foreach ( $items as $slide ) {

							$image    = $slide['image'];
							$image_id = $image['ID'];
							$caption  = $slide['titulo_del_slide'];
							$url      = $slide['slide_url'];
							?>
							<div class="c-image-slider__slide js-slider__slide">
								<?php
								if ( ! empty( $url ) ) {
									echo '<a href="' . esc_url( $url ) . '">';
								}
								?>
								<div class="c-image-slider__image-wrapper">
									<?php
									echo wp_get_attachment_image(
										$image_id,
										'large',
										'',
										array(
											'class' => 'c-image-slider__image',
										)
									);
									?>
								</div>
								<?php
								if ( ! empty( $caption ) ) {
									?>
									<div class="c-image-slider__footer">
										<div class="c-card__arrow">
											<img src="<?php echo get_template_directory_uri() . '/assets/img/goto.svg' ?>"
												 class="c-card__arrow-image">
										</div>
										<div class="c-image-slider__footer-inner">
											<p class="c-image-slider__caption">
												<?php echo esc_html( $caption ); ?>
											</p>
										</div>
									</div>
									<?php
								}

								if ( ! empty( $url ) ) {
									echo '</a>';
								}
								?>
							</div>
							<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
}
