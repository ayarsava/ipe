<?php
/**
 * Newsletter block
 */


if ( ! empty( $args['block'] ) ) {
	$block       = $args['block'];
	$block_title = $block['titulo'];
	$description = $block['descripcion'];
	$shortcode   = $block['shortcode_del_formulario'];

	if ( $shortcode ) {
		?>

		<section class="c-newsletter-block o-section">
			<div class="c-newsletter-block__container">
				<div class="c-newsletter-block__inner">
					<div class="c-newsletter-block__text">
						<?php
						if ( ! empty( $block_title ) ) {
							?>
							<div class="c-newsletter-block__title">
								<?php echo esc_html( $block_title ); ?>
							</div>
							<?php
						}

						if ( ! empty( $description ) ) {
							?>
							<label for="mc4wp_email" class="c-newsletter-block__description">
								<?php echo wp_kses( $description, 'post' ); ?>
							</label>
							<?php
						}
						?>
					</div>
					<?php
					if ( ! empty( $shortcode ) ) {
						?>
						<div class="c-newsletter-block__shortcode">
							<?php
							echo do_shortcode( $shortcode );
							?>
						</div>
						<?php
					}
					?>

				</div>
			</div>

		</section>
		<?php
	}
}
