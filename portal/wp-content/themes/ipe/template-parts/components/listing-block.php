<?php
/**
 * Listing block
 */

if ( ! empty( $args['block'] ) ) {
	$block = $args['block'];

	$block_title           = $block['title'];
	$description           = $block['description'];
	$disable_images        = $args['disable-images'] ?? false;
	$description           = $block['description'] ?? '';
	$content_types_to_show = $block['content_types_to_show'] ?: array( 'post' );
	$events_to_show        = $block['events_to_show'];

	$enable_keyword_searching = $block['enable_keyword_searching'] ?? false;
	$filters_to_show          = $block['filters_to_show'];

	if ( isset( $_GET['str'] ) ) {
		$str = $_GET['str'];
	} else {
		$str = '';
	}

	if ( isset( $_GET['post_tag'] ) ) {
		$post_tag = $_GET['post_tag'];
	} else {
		$post_tag = null;
	}


	//$category       = get_request_parameter( 'category', );
	//$post_tag       = get_request_parameter( 'post_tag', );
	$posts_per_page = 12;
	$paged          = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	/*if ( isset( $_GET['tipos_de_contenido'] ) || isset( $_GET['tipos_de_articulo'] ) || isset( $_GET['category'] ) ) {
		$tax_query = array( 'relation' => 'AND' );
	} else {
		$tax_query = array( 'relation' => 'OR' );
	}*/

	// CATEGORY
	$categories_to_show = $block['categories_to_show'];
	if ( $categories_to_show ) {
		$categories = array();
		foreach ( $categories_to_show as $term ):
			$categories[] = $term->slug;
		endforeach;
	}

	//if ( isset( $filters_to_show ) && in_array( 'category', $filters_to_show ) ) {
	if ( $categories_to_show ) {
		if ( isset( $_GET['category'] ) ) {
			$category = $_GET['category'];
		}/* elseif ( isset( $_GET['tipos_de_contenido'] ) || isset( $_GET['tipos_de_articulo'] ) ) {
			$category = '';
		}*/

		$categories_to_show = $category ?? $categories;

		if ( isset( $categories_to_show ) ) {
			$cat_query = array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => $categories_to_show,
				'operator' => 'IN',
			);
		}
	}

	// TIPOS DE CONTENIDO
	$tipos_de_contenido_to_show = $block['tipos_de_contenido_to_show'];

	// if ( isset( $filters_to_show ) && $tipos_de_contenido_to_show && in_array( "tipos_de_contenido", $filters_to_show ) ) {

	$tdc = array();
	if ( $tipos_de_contenido_to_show ) {
		foreach ( $tipos_de_contenido_to_show as $term ):
			$tdc[] = $term->slug;
		endforeach;


		if ( isset( $_GET['tipos_de_contenido'] ) ) {
			$tipos_de_contenido = $_GET['tipos_de_contenido'];
		}/* elseif ( isset( $_GET['category'] ) || isset( $_GET['tipos_de_articulo'] ) ) {
			$tipos_de_contenido = '';
		}*/
		$tipos_de_contenido = $tipos_de_contenido ?? $tdc;

		if ( isset( $tipos_de_contenido ) ) {
			$taxs_query[] = array(
				'taxonomy' => 'tipos-de-contenido',
				'field'    => 'slug',
				'terms'    => $tipos_de_contenido,
				'operator' => 'IN',
			);
		}
	}
	// }

	// TIPOS DE ARTICULO

	$tipos_de_articulo_to_show = $block['tipos_de_articulo_to_show'];

	// if ( isset( $filters_to_show ) && $tipos_de_articulo_to_show && in_array( "tipos_de_articulo", $filters_to_show ) ) {

	$tda = array();
	if ( $tipos_de_articulo_to_show ) {
		foreach ( $tipos_de_articulo_to_show as $term ):
			$tda[] = $term->slug;
		endforeach;


		if ( isset( $_GET['tipos_de_articulo'] ) ) {
			$tipos_de_articulo = $_GET['tipos_de_articulo'];
		}/* elseif ( isset( $_GET['category'] ) || isset( $_GET['tipos_de_contenido'] ) ) {
			$tipos_de_articulo = '';
		}*/
		$tipos_de_articulo = $tipos_de_articulo ?? $tda;

		if ( isset( $tipos_de_articulo ) ) {
			$taxs_query[] = array(
				'taxonomy' => 'tipos-de-articulo',
				'field'    => 'slug',
				'terms'    => $tipos_de_articulo,
				'operator' => 'IN',
			);
		}
	}
	//}
	/*
		if ( isset( $post_tag ) ) {
			$tax_query[] = array(
				'taxonomy' => 'post_tag',
				'field'    => 'slug',
				'terms'    => $post_tag,
				'relation' => 'OR',
			);
		}
	*/
	if ( isset( $_GET['tipos_de_contenido'] ) || isset( $_GET['tipos_de_articulo'] ) ) {
		$relation = 'AND';
	} else {
		$relation = 'OR';
	}


	$args = [
		'post_title_like' => $str,
		'post_type'       => $content_types_to_show,
		'posts_per_page'  => $posts_per_page,
		'paged'           => $paged,
//		'tax_query'       => $taxonomy_query,
	];


	if ( ! isset( $cat_query ) ) {
		$cat_query = array();
	}
	if ( ! isset( $taxs_query ) ) {
		$taxs_query = array();
	}

	if ( ! empty( $cat_query ) || ! empty( $taxs_query ) ) {
		//$taxonomy_query = array(
		$args['tax_query'] = array(
			'relation'  => $relation,
			'tax_query' => $cat_query,
			array(
				'relation'  => 'OR',
				'tax_query' => $taxs_query,
			),
		);
	};

	if ( count( $content_types_to_show ) === 1 ) {
		switch ( $content_types_to_show[0] ) {
			case 'evento':
				if ( ! empty( $events_to_show ) ) {
					$today = date( 'Ymd' );
					if ( 'future' === $events_to_show ) {
						$args['orderby']      = 'meta_value';
						$args['meta_query'][] = array(
							'key'     => 'fecha_de_inicio',
							'value'   => $today,
							'compare' => '>=',
						);
						$args['order']        = 'ASC';
					} elseif ( 'past' === $events_to_show ) {
						$args['orderby']      = 'meta_value';
						$args['meta_query'][] = array(
							'key'     => 'fecha_de_inicio',
							'value'   => $today,
							'compare' => '<',
						);
						$args['order']        = 'ASC';
					} else {
						unset( $args['meta_query'] );
						unset( $args['order'] );
						unset( $args['orderby'] );
					}
				}
		}
	}

	$query = new WP_Query( $args );

	/*echo '<pre>';
	var_dump( $args );
	echo '</pre>';*/

	?>

	<section class="c-listing-block o-section">
		<div class="c-listing-block__container o-container">
			<div class="c-listing-block__inner<?php if ( ! $filters_to_show ) {
				echo ' c-listing-block__inner--no-filters';
			}
			?>">

				<div class="c-listing-block__heading">
					<?php

					if ( ! empty( $block_title ) ) {
						?>

						<h2 class="c-listing-block__title">
							<?php echo esc_html( $block_title ); ?>
						</h2>

						<?php
						if ( ! empty( $description ) ) {
							?>
							<div class="c-listing-block__description"><?php echo esc_html( $description ); ?></div>
							<?php
						}
						?>
						<?php
					}
					?>
				</div>

				<?php if ( $filters_to_show ) {
					?>
					<div class="c-listing-block__filters-wrapper">
						<section class="c-filters js-filters">
							<form class="c-filters__form js-filters__form" method="get" action="">

								<?php
								if ( $enable_keyword_searching ) {
									?>
									<div class="c-listing__input-wrapper">
										<input class="c-listing__input" id="search-input" name="str" type="text"
											   value="<?php echo $str; ?>"
											   placeholder="<?php echo esc_attr( __( 'Palabra clave' ) ); ?>"/>
										<button class="c-header__submit" title="Buscar" type="submit"
												aria-label="Buscar"><img
													src="<?php echo get_template_directory_uri() . '/assets/img/arrow.svg' ?>"
													height="14">
										</button>
									</div>
									<?php
								}
								?>
								<div class="c-filters__filter c-filters__filter--topics-container js-filters__filter">
									<?php
									foreach ( $filters_to_show as $filter_to_show ) {
										?>
										<div class="c-filters__filter-heading" type="button" id="<?php
										echo $filter_to_show;
										?>">
											<?php
											// TODO: Agregar títulos segun filters_to_show.
											if ( 'category' === $filter_to_show ) {
												echo 'Categorías';
											} elseif ( 'tipos_de_contenido' === $filter_to_show ) {
												echo 'Tipos de contenido';
											} elseif ( 'tipos_de_articulo' === $filter_to_show ) {
												echo 'Tipos de artículos';
											}
											?>
										</div>
										<ul class="c-filters__list c-filters__list--category js-filters__list">
											<?php
											// TODO: Agregar field según filters_to_show.
											if ( 'category' === $filter_to_show ) {
												$contenido_a_mostrar = $block['categories_to_show'];
											} elseif ( 'tipos_de_contenido' === $filter_to_show ) {
												$contenido_a_mostrar = $block['tipos_de_contenido_to_show'];
											} elseif ( 'tipos_de_articulo' === $filter_to_show ) {
												$contenido_a_mostrar = $block['tipos_de_articulo_to_show'];
											}
											if ( $contenido_a_mostrar != null ) {
												foreach ( $contenido_a_mostrar as $clave => $valor ) {
													$term_name = get_term( $valor )->name;
													$term_slug = get_term( $valor )->slug;
													$term_id   = $valor;
													?>
													<li class="c-filters__list-item c-filters__list-item--checkbox <?php echo $term_slug; ?> js-filters__list-item">
														<label class="c-filters__label js-filters__label"
															   for="<?php echo $filter_to_show . '-' . $term_slug; ?>"><?php echo $term_name; ?>
															<input id="<?php echo $filter_to_show . '-' . $term_slug; ?>"
																   name="<?php echo $filter_to_show; ?>[]"
																   type="checkbox"
																   value="<?php echo $term_slug; ?>"
																<?php
																if ( isset( $_GET[ $filter_to_show ] ) && in_array( $term_slug, $_GET[ $filter_to_show ] ) ) {
																	echo 'checked';
																} ?>>
															<span class="c-filters__checkmark"></span>
														</label>
													</li>
													<?php
												}
											}
											?>
										</ul>
										<?php
									}
									?>

								</div>

								<button class="c-listing__submit o-button" title="Search all content" type="submit"
										aria-label="Search"><?php echo esc_attr( __( 'Aplicar' ) ); ?>
								</button>
							</form>
						</section>
					</div>
					<?php
				}
				?>

				<div class="c-listing-block__list" id="listado">
					<?php
					// The Loop
					if ( $query->have_posts() ) {

						?>
						<div class="c-listing-block__items">
							<?php
							while ( $query->have_posts() ) {
								$query->the_post();
								$card_args = array(
									'pid'            => get_the_ID(),
									'class'          => 'c-listing-block__item',
									'disable-images' => $disable_images,
								);

								get_template_part( 'template-parts/card', null, $card_args );
							}
							?>
						</div>
						<?php
					} else {
						// no posts found
						echo '<div class="o-content-from-editor"><p>';
						echo 'No hay resultados para su búsqueda. Intentelo nuevamente eliminando fitros.';
						echo '</p></div>';
					}
					/* Restore original Post Data */
					wp_reset_postdata();
					?>
				</div>
				<nav class="c-listing-block__pagination o-section">
					<?php
					pagination_bar( $query );
					?>
				</nav>
			</div>
		</div>
		</div>
	</section>
	<?php


}
