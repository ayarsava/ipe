<?php
if ( ! empty( $args['block'] ) ) {
	$block           = $args['block'];
	$classes         = $args['classes'] ?? '';
	$layout          = $block['layout'] ?: 'one';
	$block_title = $block['block_title'];
	$wysiwyg_editor  = $block['wysiwyg_editor'];
	$wysiwyg_editor2 = $block['wysiwyg_editor_2'];
	$button          = $block['button'];

	if ( ! empty( $args['modifier'] ) ) {
		$classes .= ' c-text-block--' . $args['modifier'];
	}

	if ( ! empty( $wysiwyg_editor ) || ! empty( $wysiwyg_editor2 ) ) {
		?>
		<section
				class="c-text-block <?php echo esc_attr( $classes ); ?> o-section o-section--no-background<?php if ( 'two' === $layout ) {
					echo ' c-text-block--two-columns';
				} else {
					echo ' c-text-block--one-column';
				} ?>">

			<div class="c-text-block__container">
				<?php
				if ( ! empty( $block_title ) ) {
				?>
				<div class="c-text-block__title">
					<?php echo esc_html( $block_title ); ?>
				</div>
				<?php
				}
				if ( ! empty( $wysiwyg_editor ) ) {
					?>
					<div class="c-text-block__inner o-content-from-editor">
						<?php echo wp_kses( $wysiwyg_editor, 'post' ); ?>
					</div>
					<?php
				}
				if ( ! empty( $wysiwyg_editor2 ) ) {
					?>
					<div class="c-text-block__inner o-content-from-editor">
						<?php echo wp_kses( $wysiwyg_editor2, 'post' ); ?>
					</div>
					<?php

				}
				?>
			</div>

			<?php
			if ( $button ) {
				?>
				<div class="c-text-block__button-container o-container">
					<a class="c-text-block__button o-button" href="<?php echo esc_attr( $button['url'] ) ?>"
					   target="<?php echo esc_attr( $button['target'] ) ?>"
					   title="<?php echo esc_attr( $button['title'] ); ?>">
						<?php echo esc_html( $button['title'] ); ?>
					</a>
				</div>
				<?php
			}
			?>
		</section>
		<?php
	}
}
