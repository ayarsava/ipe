<?php
/**
 * Homepage banner
 */

$pid        = get_the_ID();
$statement  = get_field( 'statement', $pid );
$hero_title = get_field( 'title', $pid );

if ( ! empty( $statement ) ) {
	?>
	<header class="c-home-banner">
		<div class="c-home-banner__container o-container">
			<div class="c-home-banner__inner">
				<h1 class="c-home-banner__title">
					<?php echo wp_kses( $hero_title, 'post' ); ?>
				</h1>
				<div class="c-home-banner__description">
					<?php
					echo wp_kses(
						$statement,
						array(
							'br'     => array(),
							'b'      => array(),
							'em'     => array(),
							'strong' => array(),
						)
					);
					?>
				</div>
			</div>
		</div>

	</header>
	<?php
}
