<?php
$post_to_output = $args['post_to_output'];

if ( $post_to_output instanceof WP_Post || $post_to_output instanceof stdClass ) {
	$pid               = $post_to_output->ID;
	$post_type_content = get_post_type( $pid );
} else {
	$pid               = $post_to_output;
	$post_type_content = get_post_type( $pid );
}

$classes          = $args['classes'] ?? '';
$item_title       = get_the_title( $pid );
$item_link        = get_permalink( $pid );
$image            = get_the_post_thumbnail( $pid, 'thumbnail' );
$card_type        = get_post_type_label( $pid );
$date_format      = get_option( 'date_format' );
$post_date        = get_the_date( $date_format, $pid );
$publication_date = get_field( 'publication_date', $pid );

if ( ! empty( $publication_date ) ) {
	$post_date = $publication_date;
}

if ( ! empty( $image ) ) {
	$classes .= ' c-list-item--has-image ';
} else {
	$classes .= ' c-list-item--no-image ';
}

$classes .= ' c-list-item--' . $post_type_content;

// Event fields.
if ( 'event' === $post_type_content ) {
	$time_zone     = get_field( 'time_zone', $pid );
	$type_of_event = get_field( 'type_of_event', $pid );
	$start_date    = strtotime( get_field( 'start_date', $pid ) );
	$end_date      = strtotime( get_field( 'end_date', $pid ) );
}

// Survey resource.
if ( 'survey-resource' === $post_type_content ) {
	$country         = post_taxonomy_label_list( $pid, 'countries' );
	$total_countries = get_field( 'total_countries', $pid );
}

// Publications.
if ( 'publication' === $post_type_content || 'post' === $post_type_content ) {
	$authors = get_authors( $pid );
	$country = post_taxonomy_label_list( $pid, 'countries' );
}

// Feature.
if ( 'feature' === $post_type_content ) {
	$authors = get_authors( $pid );
}

$has_meta = in_array( $post_type_content, array( 'event', 'survey-resource', 'publication', 'post', 'feature' ), true );
?>

<article class="c-list-item <?php echo esc_attr( $classes ); ?>">
	<a href="<?php echo esc_attr( $item_link ); ?>" class="c-list-item__button" title="<?php echo esc_attr( $item_title ); ?>">
		<span class="c-list-item__arrow"><?php get_template_part( 'assets/views/svg', null, array( 'icon' => 'right-arrow' ) ); ?></span>

		<div class="c-list-item__type">
			<?php echo esc_html( $card_type ); ?>
		</div>

		<h3 class="c-list-item__heading">
			<?php echo esc_html( $item_title ); ?>
		</h3>

		<?php
		if ( $has_meta ) {
			?>
			<div class="c-list-item__meta">
				<?php
				if ( 'event' === $post_type_content ) {
					if ( ! empty( $start_date ) ) {
						echo '<span>';
						echo date_i18n( "d M Y, g:iA", $start_date );
					}
					if ( ! empty( $end_date ) ) {
						echo " – ";
						echo date_i18n( "d M Y, g:iA", $end_date );
						echo '</span>';
					}
					if ( ! empty( $type_of_event ) ) {
						echo '<span>';
						echo esc_attr( $type_of_event['label'] );
						echo '</span>';
					}
					if ( ! empty( $time_zone ) ) {
						echo '<span>';
						echo esc_html( $time_zone );
						echo '</span>';
					}
				}

				if ( 'survey-resource' === $post_type_content ) {
					if ( ! empty( $start_date ) ) {
						echo '<span>';
						echo date_i18n( "d F Y, g:iA", $start_date );
						echo '</span>';
					}

					echo '<span>';
					echo esc_html( $post_date );
					echo '</span>';

					if ( ! empty( $country ) ) {
						echo '<span>';
						echo esc_html( $country );
						echo '</span>';
					}
					if ( ! empty( $total_countries ) ) {
						echo '<span>';
						echo esc_html( $total_countries ) . ' total countries.';
						echo '</span>';
					}
				}

				if ( 'publication' === $post_type_content || 'post' === $post_type_content ) {
					if ( ! empty( $authors ) ) {
						foreach ( $authors as $author ) {
							if ( ! empty( $author['name'] ) ) {
								echo '<span>' . $author['name'] . '</span>';
							}
						}
					}

					echo '<span>';
					echo esc_html( $post_date );
					echo '</span>';

					if ( ! empty( $country ) ) {
						echo '<span>';
						echo esc_html( $country );
						echo '</span>';
					}
				}

				if ( 'feature' === $post_type_content ) {
					if ( ! empty( $authors ) ) {
						foreach ( $authors as $author ) {
							if ( ! empty( $author['name'] ) ) {
								echo '<span>' . $author['name'] . '</span>';
							}
						}
					}

					echo '<span>';
					echo esc_html( $post_date );
					echo '</span>';
				}
				?>
			</div>
			<?php
		}

		switch ( $post_to_output->post_type ) {
			case 'people':
				$description = get_field( 'short_bio', $pid );
				break;

			case 'feature':
			case 'organisation':
			case 'publication':
			case 'survey-resource':
			case 'event':
			case 'post':
				$description = get_field( 'short_description', $pid );
				break;
			case 'page':
				$description = get_page_builder_excerpt( $pid );
				break;
		}

		if ( ! empty( $description ) ) {
			$arr = array(
				'br'     => array(),
				'p'      => array(),
				'strong' => array(),
			);
			?>
			<p class="c-list-item__desc"><?php echo wp_kses( $description, $arr ); ?></p>
			<?php
		}

		// Relevanssi data - (only visible to admins).
		if ( current_user_can( 'manage_options' ) && isset( $post_to_output->relevance_score ) ) {
			?>
			<p class="c-list-item__weight js-relevance-score" title="The relevance rating is only shown to logged in admin and webmaster users, click to hide.">
				Relevance score: <?php echo esc_html( $post_to_output->relevance_score ); ?>
			</p>
			<?php
		}
		?>
	</a>
</article>
