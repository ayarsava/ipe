<div class="c-sidebar__card-block">
	<div class="c-sidebar__card-block-title">
		Últimas publicaciones
	</div>
	<div class="c-sidebar__card-block-cards">
		<?php
		$args = array(
			'posts_per_page' => 3,
			'category_name'  => 'publicaciones',
			'post__not_in'   => array( get_the_ID() ),
		);

		$posts = new WP_Query( $args );
		$posts = $posts->get_posts();
		foreach ( $posts as $post ) {
			$card_args = array(
				'pid' => $post->ID,
			);
			get_template_part( 'template-parts/card', null, $card_args );
		}
		wp_reset_postdata();
		?>
	</div>


	<div class="c-sidebar__card-block-title c-sidebar__card-block-title--related-content">
		Contenido relacionado
	</div>
	<div class="c-sidebar__card-block-cards c-sidebar__card-block-cards--related-content">

		<?php
		$pid             = $args['post_id'] ?? get_the_ID();
		$excluded_items  = $args['excluded_items'] ?? array();
		$related_content = get_related_content( $pid, 3, $excluded_items );
		$description     = get_field( 'description_related_content', $pid ) ?? '';
		$items           = $related_content;

		if ( ! empty( $related_content ) && is_array( $related_content ) ) {
			foreach ( $items as $post ) {
				$card_args = array(
					'pid' => $post,
				);
				get_template_part( 'template-parts/card', null, $card_args );
			}
			wp_reset_postdata();
		}
		?>
	</div>
</div>