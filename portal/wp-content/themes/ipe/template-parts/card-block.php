<?php

$layout    = $args['layout'] ?? '';
$title_pos = null;

switch ( $layout ) {
	case 'one':
		$class = '--x2-scroll';
		break;
	case 'two':
		$class = '--x2';
		break;
	case 'three':
		$class     = '--x3';
		$title_pos = $args['title_position'];
		break;
	case 'four':
		$class = '--x2-scroll';
		break;
	case 'standard':
		$class = '--x1';
		break;
	default:
		$class = '--x3';
}
$title_position = $args['title_position'] ?? 'full';

if ( ! empty( $args['items'] ) ) {
	$block_title    = $args['title'] ?? false;
	$items          = $args['items'];
	$button         = $args['button'] ?? false;
	$disable_images = $args['disable-images'] ?? false;
	$description    = $args['description'] ?? '';
	$disable_desc   = false;
	$background     = $args['background'] ?? false;
	?>
	<section class="c-card-block c-card-block--<?php echo esc_attr( $layout );
	if ( $background ) {
		echo ' c-card-block--background';
	} ?> o-section">
		<div class="c-card-block__container">
			<div class="c-card-block__heading-wrapper">
				<?php
				if ( ! empty( $block_title ) ) {
					?>
					<h2 class="c-card-block__title <?php echo esc_attr( $class ); ?>">
						<?php echo esc_html( $block_title ); ?>
					</h2>
					<?php
				}

				if ( ! empty( $description ) ) {
					?>
					<p class="c-card-block__sub-title">
						<?php echo esc_html( $description ); ?>
					</p>
					<?php
				}
				?>
				<?php
				if ( ! empty( $button ) ) {
					?>
					<div class="c-card-block__button-wrapper">
						<a class="c-card-block__button o-button"
						   href="<?php echo esc_url( $button['url'] ); ?>"
						   target="<?php echo esc_attr( $button['target'] ); ?>"
						   title="<?php echo esc_attr( $button['title'] ); ?>">
							<?php
							echo esc_html( $button['title'] );

							get_template_part( 'assets/views/svg', null, array( 'icon' => 'arrow' ) );
							?>
						</a>
					</div>
					<?php
				} ?>
			</div>
			<div class="c-card-block__cards-wrapper <?php echo esc_attr( 'c-card-block__cards-wrapper' . $class ); ?>">
				<?php
				$n      = 1;
				$length = count( array( $items ) );

				foreach ( $items

				as $item_id ) {
				if ( ( 'one' === $layout && 1 === $n ) || ( 'two' === $layout && 1 === $n ) ) {
				?>
				<div class="c-card-block__cards-wrapper--x4-left js-content-height">
					<?php
					} elseif ( 'three' === $layout && 1 === $n )  {
					$disable_desc = true;
					?>
					<div class="c-card-block__cards-wrapper--x3-right js-content-height">
						<?php
						} elseif ( 'four' === $layout && 1 === $n )  {
						$disable_images = true;
						$disable_desc   = false;
						?>
						<div class="c-card-block__cards-wrapper--x3-left js-content-height">
							<?php
							} elseif ( ( 'one' === $layout && 3 === $n ) || ( 'two' === $layout && 2 === $n ) ) {
							$class        = '--y-scroll';
							$disable_desc = false;
							?>
							<div class="c-card-block__cards-wrapper--x2-right c-card-block__cards-wrapper--scroll-y js-scroll-height">
								<div class="c-card-block__cards-wrapper--x2-right-container">
									<?php
									} elseif ( 'three' === $layout && 2 === $n ) {
									$disable_desc = false;
									?>
									<div class="c-card-block__cards-wrapper--x6-bottom c-card-block__cards-wrapper--scroll-x js-scroll-width">
										<div class="c-card-block__cards-wrapper--x6-bottom-container">
											<?php
											} elseif ( 'four' === $layout && 5 === $n ) {
											?>
											<div class="c-card-block__cards-wrapper--x3-right c-card-block__cards-wrapper--scroll-y js-scroll-height">
												<div class="c-card-block__cards-wrapper--x3-right-container">
													<?php
													}
													$post_type_slug = get_post_type( $item_id );
													$card_args      = array(
														'pid'            => $item_id,
														'class'          => $class,
														'disable-images' => $disable_images,
														'disable-desc'   => $disable_desc,
													);

													get_template_part( 'template-parts/card', null, $card_args );

													if ( ( 'one' === $layout && 2 === $n ) || ( 'two' === $layout && 1 === $n ) || ( 'three' === $layout && 1 === $n ) || ( 'four' === $layout && 4 === $n ) ) {
													?>
												</div>
												<?php
												} elseif ( ( 'one' === $layout && 2 < $n && $length === $n ) || ( 'two' === $layout && 1 < $n && $length === $n ) || ( 'three' === $layout && 1 < $n && $length === $n ) || ( 'four' === $layout && 4 < $n && $length === $n ) ) {
												?>
											</div>
										</div>
										<?php
										}
										$n ++;
										}
										?>
									</div>
								</div>

	</section>

	<?php
}
