/**
 * File custom.js.
 *
 * Handles custom functions
 */

(function ($) {

    $(window).on('load', function () {
        const marginTop = 102;
        //const contentHeight = $('.js-content-height').height();
        $('.js-content-height').each(function () {
            const contentHeight = $(this).height();
            $(this).closest('.js-content-height').siblings('.js-scroll-height').css('height', contentHeight + marginTop + 'px');
        });
    });
    // $(".js-scroll-height").css({'height': ($(".js-content-height").height() + 'px')});

    // $(".js-scroll-height").height($(".js-content-height").height());

    // console.log(contentHeight);
    //$('.js-scroll-height').height(contentHeight);
    $('iframe').wrap("<div class='iframe-container'></div>");

    // Header
    $('.js-burger-button').click(function () {
        $(".c-main-menu__container").toggleClass("is-open");
        $(".js-burger-button").toggleClass("is-open");
        $(".c-header__contents").toggleClass("is-open");
        $(".c-header").toggleClass("is-open");
    });

    // Search Toggle //
    $(".js-search-open").click(function () {
        $(".c-main-menu__container").toggleClass("active");
        $(".c-header__search_box").toggleClass("c-header__search_box--active");
        $(".c-header__search__input").focus();
    });

    // Search Toggle //
    $(".c-header__search-mobile").click(function () {
        $(".c-header__search_box--mobile").toggleClass("is-open");
        $(".c-header__search__input").focus();
    });

    $(document).on('keyup', function (event) {
        if (event.keyCode === 27) {
            if ($(".c-main-menu__container").hasClass('active') || $(".c-header__search_box--mobile").hasClass('is-open')) {
                $(".c-main-menu__container").removeClass("active");
                $(".c-header__search_box").removeClass("c-header__search_box--active");
            }
            $(".c-header__search_box--mobile").removeClass("is-open");
        }
    });

    $(document).click(function(e) {
        const target = e.target;

        if (! $(target).is('.c-header__inner') && ! $(target).parents().is('.c-header__inner')) {
            $(".c-header__search_box--mobile").removeClass("is-open");
        }
    });
    // Sticky
    /*$(window).scroll(function () {
        if ($(this).scrollTop() > 113) {
            $('.js-header').removeClass('c-header__non-sticky').addClass("c-header__sticky");
        } else {
            $('.js-header').removeClass("c-header__sticky").addClass('c-header__non-sticky');
        }
    });*/

    this.$document = $(document);
    this.$window = $(window);
    this.$body = $('body');
    this.$header = $('.js-header');
    this.$content = $('.js-content');
    this.headerHeight = this.$header.outerHeight(true);
    $(window).scroll(function () {
        if (this.$content.length) {
            // Check if the page is using the WordPress admin bar. If it is, take the height of it away from the scrollTop to prevent jumpiness
            let windowScrollTop;

            if (this.$body.hasClass('admin-bar')) {
                windowScrollTop = this.$window.scrollTop() - 32;
            } else {
                windowScrollTop = this.$window.scrollTop();
            }
            if (windowScrollTop > (this.headerHeight)) {
                if (!this.$header.hasClass('is-sticky')) {
                    // Add class and negative top position to snap the sticky header out of view (64px is the height of the sticky header)
                    this.$header.addClass('is-sticky').css('top', '-96px');
                    // Reset the top position to 0 after the length of the sticky animation so it scrolls in and out of view properly
                    setTimeout(() => {
                        this.$header.css('top', 0);
                    }, 500);
                    this.$content.css('paddingTop', this.headerHeight);
                }
            } else {
                this.$header.removeClass('is-sticky');
                this.$content.css('paddingTop', 0);
            }

            if (windowScrollTop > (this.headerHeight + this.headerHeight)) {
                if (!this.$header.hasClass('sticky-is-visible')) {
                    this.$header.addClass('sticky-is-visible').css('top', 0);
                }
            } else {
                this.$header.removeClass('sticky-is-visible');
            }
        }
    });
})(jQuery);
