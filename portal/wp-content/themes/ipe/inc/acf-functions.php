<?php
/**
 * Advanced custom fields customisations, hooks and filters.
 */

/**
 * Make sure the ACF Pro plugin is active (the theme requires it).
 */
if ( function_exists( 'is_plugin_active' ) ) {
	$is_acf_active = is_plugin_active( 'advanced-custom-fields-pro/acf.php' );

	if ( ! $is_acf_active ) {
		activate_plugin( 'advanced-custom-fields-pro/acf.php' );
	}
}

/**
 * Enable the ACF options page
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page(
		array(
			'page_title' => 'Opciones',
			'slug'       => 'custom-options',
			'capability' => 'manage_options',
		)
	);

	acf_add_options_sub_page(
		array(
			'title'      => 'Contacto',
			'slug'       => 'contacto',
			'parent'     => 'custom-options',
			'capability' => 'manage_options',
		)
	);

	acf_add_options_sub_page(
		array(
			'title'      => 'Legales',
			'slug'       => 'legales',
			'parent'     => 'custom-options',
			'capability' => 'manage_options',
		)
	);
}
