<?php

/**
 * Register the custom post types
 */
function sp_register_cpts() {
	register_eventos();
	//register_especialistas();
}

add_action( 'init', 'sp_register_cpts', 0 );

/**
 * Individual post type functions
 */

/**
 * Eventos
 */
function register_eventos() {
	$singular = 'Evento';
	$plural   = 'Eventos';

	// Set label names for the WP backend UI.
	$labels = array(
		'name'               => _x( $plural, 'Post Type General Name', 'text_domain' ),
		'singular_name'      => _x( $singular, 'Post Type Singular Name', 'text_domain' ),
		'menu_name'          => __( $plural, 'text_domain' ),
		'parent_item_colon'  => __( 'Superior ' . $singular, 'text_domain' ),
		'all_items'          => __( 'Todos los ' . $plural, 'text_domain' ),
		'view_item'          => __( 'Ver ' . $singular, 'text_domain' ),
		'add_new_item'       => __( 'Agregar nuevo ' . $singular, 'text_domain' ),
		'add_new'            => __( 'Agregar nuevo', 'text_domain' ),
		'edit_item'          => __( 'Editar ' . $singular, 'text_domain' ),
		'update_item'        => __( 'Actualizar ' . $singular, 'text_domain' ),
		'search_items'       => __( 'Buscar ' . $singular, 'text_domain' ),
		'not_found'          => __( 'No encontrado', 'text_domain' ),
		'not_found_in_trash' => __( 'No encontrado en la papelera', 'text_domain' ),
	);

	// Set post type options.
	$args = array(
		'label'               => __( strtolower( $plural ), 'text_domain' ),
		'description'         => __( 'Mi lista de ' . strtolower( $plural ), 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array(
			'editor',
			// tagsdiv-article-subtype'editor',
			'excerpt',
			'revisions',
			'title',
			// phpcs:disable
			//'author',
			//'custom-fields',
			//'comments',
			//'page-attributes',
			//'post-formats',
			'thumbnail',
			// phpcs:enable
		),
		'taxonomies'          => array( 'category' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 6,
		// Icons can be found at: https://developer.wordpress.org/resource/dashicons/.
		'menu_icon'           => 'dashicons-calendar',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		// 'show_in_rest' =>true, // Uncomment for Gutenberg.
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'rewrite'             => array(
			'slug'       => sanitize_title( strtolower( $singular ) ),
			'with_front' => false,
		),
	);
	register_post_type( sanitize_title( strtolower( $singular ) ), $args );
}


/**
 * Especialistas
 */
function register_especialistas() {
	$singular = 'Especialista';
	$plural   = 'Especialistas';

	// Set label names for the WP backend UI.
	$labels = array(
		'name'               => _x( $plural, 'Post Type General Name', 'text_domain' ),
		'singular_name'      => _x( $singular, 'Post Type Singular Name', 'text_domain' ),
		'menu_name'          => __( $plural, 'text_domain' ),
		'parent_item_colon'  => __( 'Superior ' . $singular, 'text_domain' ),
		'all_items'          => __( 'Todos los ' . $plural, 'text_domain' ),
		'view_item'          => __( 'Ver ' . $singular, 'text_domain' ),
		'add_new_item'       => __( 'Agregar nuevo ' . $singular, 'text_domain' ),
		'add_new'            => __( 'Agregar nuevo', 'text_domain' ),
		'edit_item'          => __( 'Editar ' . $singular, 'text_domain' ),
		'update_item'        => __( 'Actualizar ' . $singular, 'text_domain' ),
		'search_items'       => __( 'Buscar ' . $singular, 'text_domain' ),
		'not_found'          => __( 'No encontrado', 'text_domain' ),
		'not_found_in_trash' => __( 'No encontrado en la papelera', 'text_domain' ),
	);

	// Set post type options.
	$args = array(
		'label'               => __( strtolower( $plural ), 'text_domain' ),
		'description'         => __( 'Mi lista de ' . strtolower( $plural ), 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array(
			'editor',
			// 'excerpt',
			'revisions',
			'title',
			// phpcs:disable
			//'author',
			//'custom-fields',
			//'comments',
			//'page-attributes',
			//'post-formats',
			'thumbnail',
			// phpcs:enable
		),
		'taxonomies'          => array(),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 6,
		// Icons can be found at: https://developer.wordpress.org/resource/dashicons/.
		'menu_icon'           => 'dashicons-calendar',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		// 'show_in_rest' =>true, // Uncomment for Gutenberg.
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'rewrite'             => array(
			'slug'       => sanitize_title( strtolower( $singular ) ),
			'with_front' => false,
		),
	);
	register_post_type( sanitize_title( strtolower( $singular ) ), $args );
}


/**
 * Register taxonomies and create default terms.
 */
function sp_register_tax() {
	create_proyectos_taxonomy();
	create_especialistas_taxonomy();
	create_tipos_de_contenido_taxonomy();
	create_tipos_de_articulo_taxonomy();


	// Create Default Taxonomy Terms.
	sp_add_default_terms();
}

add_action( 'init', 'sp_register_tax', 0 );

/**
 * Register principles taxonomy (hierarchical like categories)
 */
function create_proyectos_taxonomy() {
	$plural   = __( 'Proyectos', 'text_domain' );
	$singular = __( 'Proyecto', 'text_domain' );

	// Set label names.
	// phpcs:disable
	$labels = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'search_items'      => sprintf( __( 'Buscar %s', 'text_domain' ), $plural ),
		'all_items'         => sprintf( __( 'Todos los %s', 'text_domain' ), $plural ),
		'parent_item'       => sprintf( __( '%s superior', 'text_domain' ), $singular ),
		'parent_item_colon' => sprintf( __( '%s superior:', 'text_domain' ), $plural ),
		'edit_item'         => sprintf( __( 'Editar %s', 'text_domain' ), $singular ),
		'update_item'       => sprintf( __( 'Actualizar %s', 'text_domain' ), $singular ),
		'add_new_item'      => sprintf( __( 'Agregar nuevo %s', 'text_domain' ), $singular ),
		'new_item_name'     => sprintf( __( 'Nuevo nombre de %s', 'text_domain' ), $singular ),
		'menu_name'         => $plural,
	);
	// phpcs:enable

	// Register the taxonomy.
	register_taxonomy(
		sanitize_title( strtolower( $plural ) ),
		array(
			'post',
		),
		array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => false,
			// 'show_in_rest' =>true, // Uncomment for Gutenberg.
			'query_var'         => true,
			'rewrite'           => array( 'slug' => sanitize_title( strtolower( $singular ) ) ),
		)
	);
}

function create_especialistas_taxonomy() {
	$plural   = __( 'Especialistas', 'text_domain' );
	$singular = __( 'Especialista', 'text_domain' );

	// Set label names.
	// phpcs:disable
	$labels = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'search_items'      => sprintf( __( 'Buscar %s', 'text_domain' ), $plural ),
		'all_items'         => sprintf( __( 'Todos los %s', 'text_domain' ), $plural ),
		'parent_item'       => sprintf( __( '%s superior', 'text_domain' ), $singular ),
		'parent_item_colon' => sprintf( __( '%s superior:', 'text_domain' ), $plural ),
		'edit_item'         => sprintf( __( 'Editar %s', 'text_domain' ), $singular ),
		'update_item'       => sprintf( __( 'Actualizar %s', 'text_domain' ), $singular ),
		'add_new_item'      => sprintf( __( 'Agregar nuevo %s', 'text_domain' ), $singular ),
		'new_item_name'     => sprintf( __( 'Nuevo nombre de %s', 'text_domain' ), $singular ),
		'menu_name'         => $plural,
	);
	// phpcs:enable

	// Register the taxonomy.
	register_taxonomy(
		sanitize_title( strtolower( $plural ) ),
		array(
			'post',
		),
		array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => false,
			// 'show_in_rest' =>true, // Uncomment for Gutenberg.
			'query_var'         => true,
			'rewrite'           => array( 'slug' => sanitize_title( strtolower( $singular ) ) ),
		)
	);
}

function create_tipos_de_contenido_taxonomy() {
	$plural   = __( 'Tipos de contenido', 'text_domain' );
	$singular = __( 'Tipo de contenido', 'text_domain' );

	// Set label names.
	// phpcs:disable
	$labels = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'search_items'      => sprintf( __( 'Buscar %s', 'text_domain' ), $plural ),
		'all_items'         => sprintf( __( '%s', 'text_domain' ), $plural ),
		'parent_item'       => sprintf( __( '%s superior', 'text_domain' ), $singular ),
		'parent_item_colon' => sprintf( __( '%s superior:', 'text_domain' ), $plural ),
		'edit_item'         => sprintf( __( 'Editar %s', 'text_domain' ), $singular ),
		'update_item'       => sprintf( __( 'Actualizar %s', 'text_domain' ), $singular ),
		'add_new_item'      => sprintf( __( 'Agregar nuevo %s', 'text_domain' ), $singular ),
		'new_item_name'     => sprintf( __( 'Nuevo nombre de %s', 'text_domain' ), $singular ),
		'menu_name'         => $plural,
	);
	// phpcs:enable

	// Register the taxonomy.
	register_taxonomy(
		sanitize_title( strtolower( $plural ) ),
		array(
			'post',
		),
		array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => false,
			// 'show_in_rest' =>true, // Uncomment for Gutenberg.
			'query_var'         => true,
			'rewrite'           => array( 'slug' => sanitize_title( strtolower( $singular ) ) ),
		)
	);
}

function create_tipos_de_articulo_taxonomy() {
	$plural   = __( 'Tipos de artículo', 'text_domain' );
	$singular = __( 'Tipo de artículo', 'text_domain' );

	// Set label names.
	// phpcs:disable
	$labels = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'search_items'      => sprintf( __( 'Buscar %s', 'text_domain' ), $plural ),
		'all_items'         => sprintf( __( 'Todos los %s', 'text_domain' ), $plural ),
		'parent_item'       => sprintf( __( '%s superior', 'text_domain' ), $singular ),
		'parent_item_colon' => sprintf( __( '%s superior:', 'text_domain' ), $plural ),
		'edit_item'         => sprintf( __( 'Editar %s', 'text_domain' ), $singular ),
		'update_item'       => sprintf( __( 'Actualizar %s', 'text_domain' ), $singular ),
		'add_new_item'      => sprintf( __( 'Agregar nuevo %s', 'text_domain' ), $singular ),
		'new_item_name'     => sprintf( __( 'Nuevo nombre de %s', 'text_domain' ), $singular ),
		'menu_name'         => $plural,
	);
	// phpcs:enable

	// Register the taxonomy.
	register_taxonomy(
		sanitize_title( strtolower( $plural ) ),
		array(
			'post',
		),
		array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => false,
			// 'show_in_rest' =>true, // Uncomment for Gutenberg.
			'query_var'         => true,
			'rewrite'           => array( 'slug' => sanitize_title( strtolower( $singular ) ) ),
		)
	);
}


function sp_add_default_terms() {
	if ( get_option( 'initial_taxes_created' ) !== '1' ) {
		$taxonomies = array(
			'tipos-de-contenido' => array(
				'Articulo',
				'Podcast',
				'Video',
			),
			'tipos-de-articulo'  => array(
				'Articulo',
				'Publicación',
				'Proyecto',
				'Academia',
				'Prensa',
			),
		);

		foreach ( $taxonomies as $taxonomy => $terms ) {
			foreach ( $terms as $term_label ) {
				$term = term_exists( $term_label, $taxonomy );

				// If the term doesn't exist, create it .
				if ( empty( $term ) ) {
					$term = wp_insert_term(
						$term_label,
						$taxonomy,
						array(
							'description' => $term_label,
							'slug'        => sanitize_title( $term_label ),
						)
					);
				}
			}
		}

		update_option( 'initial_taxes_created', '1' );
	}
}

// Removing global taxonomies boxes via loop
function remove_taxonomy_boxes() {
	/*
	 $post_types = [
		'post',
	];

	foreach ( $post_types as $post_type ) {
		remove_meta_box( 'tagsdiv-authors', $post_type, 'side' );
		remove_meta_box( 'categorydiv', $post_type, 'side' );
		remove_meta_box( 'topicsdiv', $post_type, 'side' );
		remove_meta_box( 'seriesdiv', $post_type, 'side' );
		remove_meta_box( 'tagsdiv-countries', $post_type, 'side' );
		remove_meta_box( 'tagsdiv-languages', $post_type, 'side' );
		remove_meta_box( 'tagsdiv-regions', $post_type, 'side' );
		remove_meta_box( 'tagsdiv-survey-rounds', $post_type, 'side' );
		remove_meta_box( 'tagsdiv-keywords', $post_type, 'side' );
		remove_meta_box( 'tagsdiv-themes', $post_type, 'side' );
		remove_meta_box( "tagsdiv-$post_type-subtype", $post_type, 'side' );
	}
	*/
	remove_meta_box( "tipos-de-articulodiv", 'post', 'side' );

}

add_action( 'admin_menu', 'remove_taxonomy_boxes' );
