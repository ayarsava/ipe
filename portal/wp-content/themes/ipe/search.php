<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Instituto_Peruano_de_Economía
 */

get_header();
?>

	<main id="primary" class="site-main">
		<section>


			<header class="c-page-header">
				<div class="c-page-header__container o-container">

					<div class="c-page-header__wrapper">
						<div class="c-page-header__inner">
							<h1 class="c-page-header__heading">
								<?php
								/* translators: %s: search query. */
								printf( esc_html__( 'Búsqueda', 'ipe' ), '<span>' . get_search_query() . '</span>' );
								?>
							</h1>
						</div>
					</div>
				</div>
			</header>

			<section class="c-listing-block o-section">
				<div class="c-listing-block__container o-container">
					<div class="c-listing-block__inner">

						<div class="c-listing-block__filters-wrapper">
							<section class="c-filters js-filters">
								<?php
								$filterByCategory = get_query_var( 'category_name' );
								$filterByTdc      = get_query_var( 'tipos-de-contenido' );
								$orderBy          = get_query_var( 'orderby' );
								$order            = get_query_var( 'order' );
								?>

								<form class="c-filters__form js-filters__form" method="get" action="">
									<div class="c-listing__input-wrapper">
										<input class="c-listing__input" id="search-input" name="s" type="text"
											   value="<?php the_search_query(); ?>"
											   placeholder="Buscar"/>
										<button class="c-header__submit" title="Buscar" type="submit"
												aria-label="Buscar"><img
													src="<?php echo get_template_directory_uri() . '/assets/img/arrow.svg' ?>"
													height="14">
										</button>
									</div>

									<div class="c-filters__filter c-filters__filter--topics-container js-filters__filter">

										<div class="c-filters__filter-heading" type="button" id="category">
											Categorías
										</div>


										<select class="c-filters__list c-filters__list--select" name="category_name">

											<?php
											$args = array(
												'exclude' => '1',
											);

											$categories = get_categories( $args );

											if ( empty( $filterByCategory ) ) {
												echo '<option value="" selected>--</option>';
											} else {
												echo '<option value="">--</option>';
											}
											foreach ( $categories as $category ) {
												$selected = $category->slug === $filterByCategory ? 'selected' : '';

												echo '<option value="' . $category->slug . '" ' . $selected . '>' . $category->name . '</option>';
											}
											?>
										</select>


										<div class="c-filters__filter-heading" type="button" id="category">
											Tipo de contenido
										</div>

										<select class="c-filters__list c-filters__list--select"
												name="tipos-de-contenido">
											<?php
											$args = array(
												'taxonomy'   => 'tipos-de-contenido',
												'hide_empty' => true,
											);

											$tipos_de_contenido = get_terms( $args );
											if ( empty( $filterByTdc ) ) {
												echo '<option value="" selected>--</option>';
											} else {
												echo '<option value="">--</option>';
											}
											foreach ( $tipos_de_contenido as $tipo_de_contenido ) {
												$selected = $tipo_de_contenido->slug === $filterByTdc ? 'selected' : '';
												echo '<option value="' . $tipo_de_contenido->slug . '" ' . $selected . '>' . $tipo_de_contenido->name . '</option>';
											}
											?>
										</select>
										<div class="c-filters__filter-heading" type="button" id="order">
											Ordenar
										</div>

										<select class="c-filters__list c-filters__list--order" name="orderby"/>
										<?php
										$orderByOptions = array( 'date' => 'Fecha', 'title' => 'Título' );
										foreach ( $orderByOptions as $orderByValue => $orderByText ) {
											$selected = ( ! $orderBy && $orderByValue === 'date' ) || $orderByValue === $orderBy ? 'selected' : '';

											echo '<option value="' . $orderByValue . '" ' . $selected . '>' . $orderByText . '</option>';
										}

										?>
										</select>
										<select class="c-filters__list c-filters__list--order" name="order"/>

										<?php
										$orderOptions = array( 'DESC', 'ASC' );
										foreach ( $orderOptions as $orderOption ) {
											$selected = ( ! $order && $orderOption === 'DESC' ) || $orderOption === $order ? 'selected' : '';

											echo '<option value="' . $orderOption . '" ' . $selected . '>' . $orderOption . '</option>';
										}


										?>
										</select>

									</div>
									<button class="c-listing__submit o-button" title="Search all content" type="submit"
											aria-label="Search"><?php echo esc_attr( __( 'Aplicar' ) ); ?>
									</button>
								</form>
							</section>
						</div>

						<div class="c-listing-block__list">
							<?php
							if ( have_posts() ) :
								?>
								<div class="c-listing-block__items">
									<?php

									while ( have_posts() ) :
										the_post();
										/* Start the Loop */
										$card_args = array(
											'pid'            => get_the_ID(),
											'class'          => 'c-listing-block__item',
											'disable-images' => false,
										);

										/**
										 * Run the loop for the search to output the results.
										 * If you want to overload this in a child theme then include a file
										 * called content-search.php and that will be used instead.
										 */
										get_template_part( 'template-parts/card', null, $card_args );

									endwhile;
									?>
								</div>
							<?php

							else :
								?>
								<div class="c-listing-block__no-results">
									<?php
									echo 'No hemos encontrado resultados para su búsqueada. Intente nuevamente utitlizando menos filtros.';
									?>
								</div>
							<?php

							endif;
							?>
							<nav class="c-listing-block__pagination o-section">
								<?php
								the_posts_pagination( array(
									'mid_size'  => 2,
									'prev_text' => __( '<span class="c-list__pagination-prev">Previous</span>', 'textdomain' ),
									'next_text' => __( '<span class="c-list__pagination-next">Next</span>', 'textdomain' ),
								) );
								?>
							</nav>
						</div>
			</section>

	</main><!-- #main -->

<?php
get_footer();
