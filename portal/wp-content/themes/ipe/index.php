<?php
/**
 * Disable the index page.
 * - We don't like to use the automated index.php outputs because it is not content manageable by the client.
 * - If someone has ended up at this URL it is an error.
 */

// Force a 404.
$wp_query->set_404();
status_header( 404 );
nocache_headers();

// Include the 404 template code.
require_once '404.php';

die;
