<?php
/**
 * Template Name: Landing page
 */

get_header();

while ( have_posts() ) {
	the_post();
	?>
	<section>
		<article>
			<?php get_template_part( 'template-parts/page-header' ); ?>
			<?php
			if ( ! empty( get_the_content() ) ) {
				?>
				<div class="o-single__container o-container">
					<div class="o-single__content-wrapper">
						<div class="o-single__content<?php if (get_field('full_width')) { echo ' o-single__content--full-width'; } ?>">
							<div class="o-content-from-editor js-content-from-editor">
								<?php the_content(); ?>

								<!--<button id="create_pdf" class="o-button">Generate PDF</button>-->
							</div>
						</div>
					</div>
				</div>
				<?php
			}
			?>
			<?php get_template_part( 'template-parts/content-blocks' ); ?>
		</article>
	</section>
	<?php
}

get_footer();
